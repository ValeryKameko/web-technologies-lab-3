<?php

if (\preg_match('/^\/index.*$/', $_SERVER['REQUEST_URI'])) {
    $error = false;
    include_once __DIR__ . '/src/form.php';
}
elseif (\preg_match('/^\/form_handler.*$/', $_SERVER['REQUEST_URI'])) {
    include_once __DIR__ . '/src/form_handler.php';
}