<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Календарь на <?= $year ?>/<?= $year + 1 ?> год для <?= $course ?> курса</title>
        <link href="/css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <h1 class="title">Календарь на <?= $year ?>/<?= $year + 1 ?> год для <?= $course ?> курса</h1>
        </header>
        <hr>
        <?php include_once __DIR__ . '/calendar_view.php'; ?>
    </body>
</html>