<?php

define('STUDY_YEAR_START_MONTH', 8);
define('DAYS_IN_WEEK', 7);

function formatDateTime($dateTime, $format = NULL)
{
    $formatter = new IntlDateFormatter(
        'ru_RU', 
        IntlDateFormatter::FULL, 
        IntlDateFormatter::FULL,
        'Europe/Minsk',
        IntlDateFormatter::GREGORIAN,
        $format
    );
    return $formatter->format($dateTime);
}

function generateMonthCalendar($year, $course, $month, &$weekNumber)
{
    $monthCalendar = [];
    $calendar = new IntlGregorianCalendar($year, $month, 1);
    while ($calendar->get(IntlCalendar::FIELD_DAY_OF_WEEK) !== IntlCalendar::DOW_MONDAY)
        $calendar->add(IntlCalendar::FIELD_DAY_OF_MONTH, -1);
    
    $endWithCurrentMonth = false;
    do {
        $weekCalendar = [];
        for ($i = 0; $i < DAYS_IN_WEEK; $i++) {
            $dayOfWeek = $calendar->get(IntlCalendar::FIELD_DAY_OF_WEEK);
            $dayInfo = [
                'type' => 'none',
                'date' => $calendar->toDateTime(),
                'day' => $calendar->get(IntlCalendar::FIELD_DAY_OF_MONTH),
                'is_weekend' => $calendar->getDayOfWeekType($dayOfWeek) === IntlGregorianCalendar::DOW_TYPE_WEEKEND,
                'is_current_month' => $calendar->get(IntlCalendar::FIELD_MONTH) === $month
            ];
            $endWithCurrentMonth = $dayInfo['is_current_month'];
            array_push($weekCalendar, $dayInfo);
            $calendar->add(IntlCalendar::FIELD_DAY_OF_MONTH, +1);
        }
        $weekInfo = [
            'week' => $weekCalendar,
            'week_number' => $weekNumber
        ];
        
        $weekNumber = $weekNumber % 4 + 1;
        array_push($monthCalendar, $weekInfo);
    } while ($calendar->get(IntlCalendar::FIELD_MONTH) === $month);
    if (!$endWithCurrentMonth)
        $weekNumber = ($weekNumber + 2) % 4 + 1;
    return $monthCalendar;
}

function generateCalendar($year, $course)
{
    $yearCalendar = [];
    $weekNumber = 1;
    $calendar = new IntlGregorianCalendar($year, STUDY_YEAR_START_MONTH, 1);

    $weekdayNumbers = array_map(function ($weekdayNumber) {
        return $weekdayNumber % DAYS_IN_WEEK + 1;
    }, range($calendar->getDayOfWeekType(IntlCalendar::DOW_TYPE_WEEKEND_CEASE) + 1, DAYS_IN_WEEK));

    $weekdayNames = array_map(function ($weekDayNumber) {
        $calendar = new IntlGregorianCalendar();
        $calendar->set(IntlCalendar::FIELD_DAY_OF_WEEK, $weekDayNumber);
        return formatDateTime($calendar->toDateTime(), 'EEEE');
    }, $weekdayNumbers);
    
    do {
        $monthCalendar = generateMonthCalendar(
            $calendar->get(IntlCalendar::FIELD_YEAR),
            $course,
            $calendar->get(IntlCalendar::FIELD_MONTH),
            $weekNumber);

        $monthInfo = [
            'month' => $monthCalendar,
            'name' => formatDateTime($calendar->toDateTime(), 'LLLL')
        ];
        $calendar->add(IntlCalendar::FIELD_MONTH, +1);
        array_push($yearCalendar, $monthInfo);
    } while ($calendar->get(IntlCalendar::FIELD_MONTH) !== STUDY_YEAR_START_MONTH);

    $yearInfo = [
        'year' => $yearCalendar,
        'weekday_names' => $weekdayNames,
    ];
    markEvents($year, $course, $yearInfo);
    return $yearInfo;
}

function isInsideDayRange($date, $from, $to) {
    $localDate = DateTime::createFromFormat('d.m', $date->format('d.m'));
    if ($from <= $to)
        return ($from <= $localDate) && ($localDate <= $to);
    else
        return ($from <= $localDate) || ($localDate <= $to);
}

function markEvents($year, $course, &$calendar)
{
    $events = json_decode(file_get_contents(dirname(__DIR__) . '/data/events.json'), true);
    $eventFormat = $events['format'];

    foreach ($calendar['year'] as &$monthInfo) {
        foreach ($monthInfo['month'] as &$weekInfo) {
            foreach ($weekInfo['week'] as &$dayInfo) {
                foreach ($events['courses'][$course] as $event) {
                    $fromDay = DateTime::createFromFormat($eventFormat, $event['from']);
                    $toDay = DateTime::createFromFormat($eventFormat, $event['to']);
                    if (isInsideDayRange($dayInfo['date'], $fromDay, $toDay))
                        $dayInfo['type'] = $event['type'];
                }
            }
        }
    }
}