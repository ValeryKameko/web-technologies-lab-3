<div class="month">
    <h5 class="month-name"><?= $monthInfo['name'] ?></h5>
    <div class="weekday-name-contaner">
<?php foreach ($calendar['weekday_names'] as $weekdayName): ?>
        <div class="weekday-name">
            <span><?= $weekdayName ?></span>
        </div>
<?php endforeach; ?>
    </div>
    <div class = "month-week-number-container">
        <div class="month-container">
<?php foreach ($monthInfo['month'] as $weekInfo): ?>
<?php   include __DIR__ . '/week_view.php'; ?>
<?php endforeach; ?>
        </div>
        <hr>
        <div class="week-number-container">
<?php foreach ($monthInfo['month'] as $weekInfo): ?>
            <div class="week-number">
                <span><?= $weekInfo['week_number'] ?></span>
            </div>
<?php endforeach; ?>
        </div>
    </div>
</div>
