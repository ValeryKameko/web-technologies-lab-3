<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Форма</title>
        <link href="/css/form_style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <form action="form_handler.php" method="get" class="calendar-form">
                <div>
                    <label>Год:</label>
                    <input type="number" id="year" name="year" min="1900" max="2100" required>
                </div>
                <div>
                    <label>Курс:</label>
                    <input type="number" id="course" name="course" min="1" max="8" required>
                </div>
                <?php if ($error): ?>
                    <div class="error-container">
                        <span class="error-text"><?= $errorMessage ?></span>
                    </div>
                <?php endif; ?>
                <input type="submit" value="Показать календарь">
            </form>
        </div>
    </body>
</html>