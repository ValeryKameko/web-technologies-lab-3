<?php if ($dayInfo['is_current_month']): ?>
<div class="day<?= 
    ($dayInfo['is_weekend'] ? ' daytype-weekend' : ' daytype-weekday') .
    (' daytype-' . $dayInfo['type'])
?>">
    <span>
        <?= $dayInfo['day'] ?>
    </span>
</div>
<?php else: ?>
<div class="day">
</div>
<?php endif; ?>
