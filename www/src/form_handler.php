<?php

$yearValidationOptions = [
    'options' => [
        'min_range' => 1900,
        'max_range' => 2100
    ]
];
$courseValidationOptions = [
    'options' => [
        'min_range' => 1,
        'max_range' => 8
    ]
];

$error = false;
if (!isset($_GET['year']) || !isset($_GET['course'])) {
    header("Location: /form.php");
} 
elseif (($year = filter_var($_GET['year'], FILTER_VALIDATE_INT, $yearValidationOptions)) === FALSE) {
    $error = true;
    $errorMessage = "Год должен находится в пределах от {$yearValidationOptions['options']['min_range']} to {$yearValidationOptions['options']['max_range']}";
}
elseif (($course = filter_var($_GET['course'], FILTER_VALIDATE_INT, $courseValidationOptions)) === FALSE) {
    $error = true;
    $errorMessage = "Номер курса должен находится в пределах от {$courseValidationOptions['options']['min_range']} до {$courseValidationOptions['options']['max_range']}";
}
else {
    include_once __DIR__ . '/calendar_generator.php';
    $calendar = generateCalendar($year, $course);

    ob_start();
    include_once __DIR__ . '/page_view.php';
    $relativeFilePath = "/calendars/study_calendar_${year}_${course}.html";
    $filePath = dirname(__DIR__) . "/public${relativeFilePath}";
    $content = ob_get_contents();
    ob_end_clean();

    file_put_contents($filePath, $content);
    header("Location: $relativeFilePath");
}

if ($error) {
    include_once __DIR__ . '/form.php';
}