<section class="calendar">
<?php foreach ($calendar['year'] as $monthInfo): ?>
    <div class="month-wrapper">
        <?php include __DIR__ . '/month_view.php'; ?>
    </div>
<?php endforeach; ?>
</section>
